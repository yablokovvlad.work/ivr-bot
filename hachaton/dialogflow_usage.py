import os
import uuid
import dialogflow
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'ad804d51d098.json'
project_id = "ferrous-acronym-233311"
session_id= "1"
texts=["Хочу купить", "asdf"]
language_code="ru-RU"

def detect_intent_texts(project_id, session_id, texts, language_code):
    print("ok")
    """Returns the result of detect intent with texts as inputs.

    Using the same `session_id` between requests allows continuation
    of the conversation."""
    import dialogflow_v2 as dialogflow
    session_client = dialogflow.SessionsClient()

    session = session_client.session_path(project_id, session_id)
    print('Session path: {}\n'.format(session))

    print(len(texts))

    for text in texts:
        text_input = dialogflow.types.TextInput(
            text=text, language_code=language_code)

        query_input = dialogflow.types.QueryInput(text=text_input)

        response = session_client.detect_intent(
            session=session, query_input=query_input)

        print('=' * 20)
        print('Query text: {}'.format(response.query_result.query_text))
        print('Detected intent: {} (confidence: {})\n'.format(
            response.query_result.intent.display_name,
            response.query_result.intent_detection_confidence))
        print('Fulfillment text: {}\n'.format(
            response.query_result.fulfillment_text))

detect_intent_texts(project_id, session_id, texts, language_code)
